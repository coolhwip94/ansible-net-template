# Ansible Net Template
> This repo will hosts an `Ansible` project that can generate configurations for a network device.


## Setup
---
- Install Ansible Galaxy collection Requirements
    ```
    ansible-galaxy collection install -r collections/requirements.yml
    ```

- Review `hosts` and `ansible.cfg` files to ensure the variables fit your environment.  (ie: if you use a different file to secure your passwords, your `vault_password_file` would not be the same as this repo, adjust these settings accordingly.)
- `.vault_pass` file is used to secure the `ansible_password` using ansible-vault. (update the credentials in `group_vars/cisco-lab-devices.yml`)

## Playbooks
---
- `playbooks/generate_interface_config.yml`: This will generate a full configuration for a cisco ios interface
    - This playbook has multiple tasks, that are intended to be run individually.
    - Examples :

    Generating Full Config :
    ```
    ansible-playbook generate_interface_config.yml -t full
    ```

    Generating Invidual Config Lines :

    ```
    # This example will generate just the ipaddress configuration
    
    ansible-playbook generate_interface_config.yml -t ipaddresss

    ```
    - The config output will be in `roles/config_generator/files` 


    > The variables that define this config is in `roles/config_generator/main.yml`

- `playbooks/send_interface_config.yml`
    - This playbook is used to send the `full_config.txt` generated from the playbook above to network device
    ```
    ansible-playbook playbooks/send_interface_config.yml
    ```
    > This sends the config to the device and backs up the current running config as well.



## Templates
---
> The templates directory will hosts templates for specific parts of the config.  
> Additional templates should follow the same convention
```
roles/config_generator/templates
├── admin_state.j2
├── full_config.j2
├── interface_description.j2
└── ip_address.j2
```

## Tasks
---
> The tasks correlate with the template being used. This is used for individual configuration generation.
```
roles/config_generator/tasks
├── generate_full_config.yml
├── generate_interface_admin_state.yml
├── generate_interface_description.yml
└── generate_ip_address.yml
```


## References
---
- https://gitlab.com/coolhwip94/ansible-dev/-/tree/master/
- https://docs.ansible.com/ansible/latest/user_guide/playbooks_reuse_roles.html#role-directory-structure
